package com.arisa.midterm;

public class JettCar {
    private String car;
    private int cost;
    private int rent;

    public JettCar(String car ,int cost, int rent){
        this.car=car;
        this.cost=cost;
        this.rent=rent;
    }
    public String getCar(){
        return car;
    }
    public int getCost(){
        return cost;
    }
    public int getRent(){
        return rent;
    }
    public void print(){
        System.out.println(car + " " + rent);
    } 
}
