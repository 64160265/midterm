package com.arisa.midterm;

import java.util.Scanner;

public class KjHotelApp {
    static void printType() {
        System.out.println(" =======!>Welcome<!======= ");
        System.out.println("Type 1 Standart Room 300 Baht");
        System.out.println("Type 2 Sweet Room 500 Baht");
        System.out.println("Type 3 Family Room 1000 Baht");
        System.out.println("Type 4 to Quit");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);// ประกาศตัวแปรเพื่อใช้รับค่าจากคีย์บอร์ด
        KjHotel standart = new KjHotel(" Standart Room ",300,1);
        KjHotel sweet = new KjHotel(" Sweet Room ",500,1);
        KjHotel family = new KjHotel(" Family Room ",1000,1);

        printType();
        System.out.println("Please enter Type your Room : ");
        int Type = sc.nextInt(); 
        if (Type == 1) {
            System.out.print("Please enter your  Day : ");
            int rest = sc.nextInt();
            System.out.println("price = " + standart.getPrice() *rest + " baht ");
            System.out.println(" =======!THANKYOU!======= ");
        }else if (Type == 2) {
            System.out.print("Please enter your  Day : ");
            int rest = sc.nextInt();
            System.out.println("price = " + sweet.getPrice() *rest + " baht ");
            System.out.println(" =======!THANKYOU!======= ");
        }else if (Type == 3) {
            System.out.print("Please enter your  Day : ");
            int rest = sc.nextInt();
            System.out.println("price = " + family.getPrice() *rest + " baht ");
            System.out.println(" =======!THANKYOU!======= ");
        }else if (Type == 4) {
            System.out.println(" =======!THANKYOU!======= ");
            System.exit(0);
        }  
    }
}

