package com.arisa.midterm;

public class KjHotel {
    private String room;
    private int price;
    private int rest;

    public KjHotel (String room ,int price, int rest){
        this.room=room;
        this.price=price;
        this.rest=rest;
    }
    public String getRoom(){
        return room;
    }
    public int getPrice(){
        return price;
    }
    public int getRest(){
        return rest;
    }
    public void print(){
        System.out.println(room + " " + rest);
    } 
}
