package com.arisa.midterm;

import java.util.Scanner;

public class JettCarApp {
    static void printCategory() {
        System.out.println(" =======!JETT CARS!======= ");
        System.out.println("Category 1 ECO Car  30,000 Baht");
        System.out.println("Category 2 Sport Car 50,000 Baht");
        System.out.println("Category 3 Family Car 10,000 Baht");
        System.out.println("Category 4 To Exit <3");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        JettCar eco = new JettCar(" ECO Car ",30000,1);
        JettCar sport = new JettCar(" Sport Car ",50000,1);
        JettCar family = new JettCar(" Family Car ",10000,1);

        printCategory();
        System.out.println("Please Select Category : ");
        int Category = sc.nextInt(); 
        if (Category == 1) {
            System.out.print("Please Enter Your Rent: ");
            int rent = sc.nextInt();
            System.out.println("price = " + eco.getCost() *rent + " Baht ");
            System.out.println(" ========THANKYOU======== ");
        }else if (Category == 2) {
            System.out.print("Please Enter Your Rent: ");
            int rent = sc.nextInt();
            System.out.println("price = " + sport.getCost() *rent + " Baht ");
            System.out.println(" ========THANKYOU======== ");
        }else if (Category == 3) {
            System.out.print("Please Enter Your Rent: ");
            int rent = sc.nextInt();
            System.out.println("price = " + family.getCost() *rent + " Baht ");
            System.out.println(" ========THANKYOU======== ");
        }else if (Category == 4) {
            System.out.println(" ========THANKYOU======== ");
            System.exit(0);
        }  
    }
}